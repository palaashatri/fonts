@REM install all fonts in ./fonts directory to Windows fonts

@echo off
setlocal

set fontsdir=%windir%\fonts
set fontssrc=%cd%\fonts

echo Copying fonts from %fontssrc% to %fontsdir%
for /f "delims=" %%f in ('dir /b /a-d %fontssrc%') do (
    echo Installing %%f
    copy "%fontssrc%\%%f" "%fontsdir%" /Y > nul
)

endlocal

